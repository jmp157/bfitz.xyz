<?php require($_SERVER['DOCUMENT_ROOT'].'/scripts/bannerinfo.php'); ?>
<nav class="top-nav">
  <div class="top-nav-title">
    <!-- <h1><a id="sitetitle" href="/">Brian Fitzpatrick</a></h1> -->
    <h1>Brian Fitzpatrick</h1>
  </div>
  <ul class="top-nav-links">
    <li><a href="/">Home</a></li>
    <li><a href="/teaching/218s21">Math 218</a></li>
    <li><a href="/teaching/790s21">Math 790-92</a></li>
  </ul>
</nav>
<a href="<?php echo $BANNER_HTTP; ?>">
  <div class="banner" title="<?php echo $BANNER_TEXT; ?>"></div>
</a>
