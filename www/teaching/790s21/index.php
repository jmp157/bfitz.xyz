<!doctype html>
<?php $TITLE='Math 790: Topics in Foundational Mathematics'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h2>Matrices and Vectors</h2>

<p>This seminar will run from <?php echo date("D d-M", mktime(0, 0, 0, 1, 20,
   2021)); ?> until <?php echo date("D d-M", mktime(0, 0, 0, 2, 17, 2021)); ?>.

  <table>
    <tr>
      <th style="text-align: left">Deadline</th>
      <th style="text-align: left">Topic</th>
      <th style="text-align: left">Resources</th>
    </tr>

    <?php

     $release_date = mktime(6, 0, 0, 1, 20, 2021);
     $due_date = mktime(0, 0, 0, 1, 29, 2021);
     $due_date_formatted = date("D d-M", $due_date);

	$jsondata = file_get_contents("790s21.json");
	$json = json_decode($jsondata, true);
	$youtube_icon = "/pix/icons/youtube.png";
	$sage_icon = "/pix/icons/sage.svg";
	$assignment_icon = "/pix/icons/memo.svg";

	$release_date = mktime(6, 0, 0, 1, 20, 2021);
	$due_date = mktime(0, 0, 0, 1, 29, 2021);

	$output = "";
	$count = 0;
	foreach($json as $item) {
		if ( time() < $release_date ) {
			break; // Stops when the last day has been reached
		}
		$due_date_formatted = date("D d-M", $due_date);

		$output .= "<tr>";
		$output .= "<td>" .$due_date_formatted . "</td>";
		$output .= "<td><a href=". $item['pdf'] . ">" . $item['title'] . "</a></td>";
		$output .= "<td><a href=" . $item['youtube'] . "><img src=". $youtube_icon . "></a>";
		if (array_key_exists('sage', $item)) {
			$output .= "<a href=" . $item['sage'] . "> <img src=". $sage_icon . "></a>";
		} 
		if (array_key_exists('assignment', $item)) {
			$output .= "<a href=" . $item['assignment'] . "> <img src=". $assignment_icon . "></a>";
		}
		$output .= "</td>";
		$output .= "</tr>";
		$count++;
		if ($count % 3 == 0) {
			$release_date = strtotime("+7 day", $release_date);
			$due_date = strtotime("+7 day", $due_date);
		}
	}
	echo $output;
    ?>

  </table>


<h2>Projections and Least Squares Methods</h2>
<p>This seminar will run from <?php echo date("D d-M", mktime(0, 0, 0, 2, 22,
   2021)); ?> until <?php echo date("D d-M", mktime(0, 0, 0, 3, 17, 2021)); ?>.

  <h2>Eigenvalues and Singular Value Decomposition</h2>
<p>This seminar will run from <?php echo date("D d-M", mktime(0, 0, 0, 3, 22,
   2021)); ?> until <?php echo date("D d-M", mktime(0, 0, 0, 4, 21, 2021)); ?>.
