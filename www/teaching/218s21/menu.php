<h1>
  <?php echo $TITLE; ?>
  <a href="./" title="schedule"><img src="/pix/icons/list.svg"></a>
  <a href="./cal.php" title="calendar"><img src="/pix/icons/calendar.svg"></a>
  <a href="https://duke.zoom.us/j/99989964994?pwd=dEg1enAzOVlub2czbWtqT3pUOFZFQT09" title="zoom"><img src="/pix/icons/zoom.svg"></a>
  <a href="https://gradescope.com" title="gradescope"><img src="/pix/icons/gradescope.png"></a>
  <a href="https://gitlab.oit.duke.edu/bdf10/course-policies/-/raw/master/218s21/218s21-policies.pdf?inline=true" title="policies"><img src="/pix/icons/scroll.svg"></a>
</h1>

<p> Welcome! You have found the homepage of the Spring 2021 manifestation of
    Math 218.
