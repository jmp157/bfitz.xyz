<?php
header("Content-type: text/css; charset: UTF-8");
require($_SERVER['DOCUMENT_ROOT'].'/scripts/bannerinfo.php');
?>

:root {
    --base03: #002b36; /* background */
    --base02: #073642; /* background highlights */
    --base01: #586e75; /* comments/secondary content */
    --base00: #657b83;
    --base0: #839496; /* body text */
    --base1: #93a1a1;
    --base2: #eee8d5;
    --base3: #fdf6e3;
    --yellow: #b58900;
    --orange: #cb4b16;
    --red: #dc322f;
    --magenta: #d33682;
    --violet: #6c71c4;
    --blue: #268bd2;
    --cyan: #2aa198;
    --green: #859900;
    --font-size: 16px;
}
* {
    box-sizing: border-box;
    color: var(--base0);
    font-family: monospace;
    font-size: var(--font-size);
    line-height: 1.428;
    margin: 0px;
}
html {
    margin-bottom: 300px;
}
body {
    background-color: var(--base03);
    max-width: 1125px;
    margin: auto;
}
p {
    margin-top: 1em;
    margin-bottom: 1em;
}
a {
    color: var(--violet);
    text-decoration: none;
}
a:hover {
    color: var(--blue);
    text-decoration: underline;
}
a img {
    height: 1.75em;
}
a img:hover {
    background: rgba(0, 0, 0, 0);
    transition: .5s;
    transform: scale(2, 2);
    text-decoration: none;
}
h1 {
    color: var(--orange);
    font-size: 2em;
}
h2 {
    color: var(--green);
    font-size: 1.5em;
}
code {
    color: var(--magenta);
    font-weight: bold;
}
#sitetitle {
    color: var(--orange);
    font-size: 1em;
}
#sitetitle:hover {
    text-decoration: none;
}
.top-nav {
    width: 100%;
    display: flex;
    justify-content: space-around;
    align-items: center;
    min-height: 8vh;
}
.top-nav-title {
    width: 45%;
}
.top-nav-links {
    display: flex;
    justify-content: space-around;
    width: 55%;
}
.top-nav-links li {
    list-style: none;
}
.top-nav-links a:hover {
    /* border-bottom: var(--magenta) inset medium; */
    text-decoration: none;
    font-weight: bold;
}
.banner {
    border-radius: .3rem;
    height: 28vh; /* used to be 24vh, then 32vh */
    margin-left: -1em;
    margin-right: -1em;
    margin-bottom: 1em;
    background: url('<?php echo $BANNER_FILE ?>') no-repeat center center;
    background-size: cover;
}
table {
    border-collapse: collapse;
    min-width: 66%;
}
tr:hover {
    background-color: var(--base02);
}
