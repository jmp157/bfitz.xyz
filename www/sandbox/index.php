<!doctype html>
<?php $TITLE='Math 218: Matrices and Vector Spaces'; ?>

<title><?php echo $TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="/css/style.php">

<?php include($_SERVER['DOCUMENT_ROOT'].'/menu.php') ?>
<?php include('./menu.php') ?>

<h2>Schedule</h2>

<p>Quizzes and problem sets can be found on gradescope.

<table>
  <tr>
    <th style="text-align: left">Deadline</th>
    <th style="text-align: left">Topic</th>
    <th style="text-align: left">Resources</th>
  </tr>

  <?php $due_date = mktime(0, 0, 0, 1, 29, 2021); ?>
  <tr>
    <td><?php echo date("D d-M", $due_date); ?></td>
    <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/welcome/welcome.pdf?inline=true">Welcome!</a></td>
    <td>
      <a href="https://youtu.be/9rVbL30Xrtk"><img src="/pix/icons/youtube.png"></a>
    </td>
  </tr>


  <tr>
    <td><?php echo date("D d-M", $due_date); ?></td>
    <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/vocab/vocab.pdf?inline=true">Matrices and Vectors</a></td>
    <td>
      <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDVGsMw5PXJh0ZiFOs7gKOK"><img src="/pix/icons/youtube.png"></a>
      <a href="./vocab-sage.php"><img src="/pix/icons/sage.svg"></a>
    </td>
  </tr>


  <tr>
    <td><?php echo date("D d-M", $due_date); ?></td>
    <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/digraphs/digraphs.pdf?inline=true">Digraphs</a></td>
    <td>
      <a href="https://youtube.com/playlist?list=PLwK75AcBxZCCpUdsW05EcJkWyoMk1P1w2"><img src="/pix/icons/youtube.png"></a>
      <a href="./digraphs-sage.php"><img src="/pix/icons/sage.svg"></a>
    </td>
  </tr>


  <?php $due_date = strtotime("+7 day", $due_date); ?>
  <tr>
    <td><?php echo date("D d-M", $due_date); ?></td>
    <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/geometry/geometry.pdf?inline=true">Vector Geometry</a></td>
    <td>
      <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDuuBiIIX9_jeyQX4Owkcra"><img src="/pix/icons/youtube.png"></a>
      <a href="./geometry-sage.php"><img src="/pix/icons/sage.svg"></a>
    </td>
  </tr>


  <tr>
    <td><?php echo date("D d-M", $due_date); ?></td>
    <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/matmult/matmult.pdf?inline=true">Matrix Multiplication</a></td>
    <td>
      <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDVZyAyLGsiIiBfgUjkNHbG"><img src="/pix/icons/youtube.png"></a>
      <a href="./matmult-sage.php"><img src="/pix/icons/sage.svg"></a>
    </td>
  </tr>


  <tr>
    <td><?php echo date("D d-M", $due_date); ?></td>
    <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/rref/rref.pdf?inline=true">Row Echelon Forms</a></td>
    <td>
      <a href="https://youtube.com/playlist?list=PLwK75AcBxZCDNISmN_JY_iDB1093clEzZ"><img src="/pix/icons/youtube.png"></a>
      <a href="./rref-sage.php"><img src="/pix/icons/sage.svg"></a>
    </td>
  </tr>


  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/axb/axb.pdf?inline=true">Linear Systems</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./axb-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj.pdf?inline=true">Gauss-Jordan Elimination</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./gj-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--     <a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gj/gj-examples.pdf?inline=true"><img src="/pix/icons/memo.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/nonsing/nonsing.pdf?inline=true">Nonsingular Matrices</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./nonsing-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/ear/ear.pdf?inline=true">EA=R Factorizations</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./ear-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/palu/palu.pdf?inline=true">PA=LU Factorizations</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./palu-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/evals/evals.pdf?inline=true">Eigenvalues</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./evals-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/null/null.pdf?inline=true">Null Spaces</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./null-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/col/col.pdf?inline=true">Column Spaces</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./col-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/fundsub/fundsub.pdf?inline=true">The Four Fundamental Subspaces</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./fundsub-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/bases/bases.pdf?inline=true">Bases</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./bases-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/dim/dim.pdf?inline=true">Dimension</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./dim-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gm/gm.pdf?inline=true">Geometric Multiplicity</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./gm-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/betti/betti.pdf?inline=true">Betti Numbers</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./betti-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/orthog/orthog.pdf?inline=true">Othogonality</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./orthog-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/proj/proj.pdf?inline=true">Projections</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./proj-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/xhat/xhat.pdf?inline=true">Least Squares</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./xhat-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/gs/gs.pdf?inline=true">The Gram-Schmidt Algorithm</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./gs-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/aqr/aqr.pdf?inline=true">A=QR Factorizations</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./aqr-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det1/det1.pdf?inline=true">Determinants I</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./det1-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det2/det2.pdf?inline=true">Determinants II</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./det2-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/det3/det3.pdf?inline=true">Determinants III</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./det3-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/clx/clx.pdf?inline=true">Complex Numbers</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./clx-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/poly/poly.pdf?inline=true">Polynomial Algebra</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./poly-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/chi/chi.pdf?inline=true">The Characteristic Polynomial</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./chi-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/diag/diag.pdf?inline=true">Diagonalization</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./diag-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/exp/exp.pdf?inline=true">Matrix Exponentials</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./exp-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/spectral/spectral.pdf?inline=true">The Spectral Theorem</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./spectral-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/posdef/posdef.pdf?inline=true">Definiteness</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./posdef-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/cholesky/cholesky.pdf?inline=true">Cholesky Factorizations</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./cholesky-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->


  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/jord/jord.pdf?inline=true">Jordan Canonical Form</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./jord-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

  <!-- <?php $due_date = strtotime("+7 day", $due_date); ?> -->
  <!-- <tr> -->
  <!--   <td><?php echo date("D d-M", $due_date); ?></td> -->
  <!--   <td><a href="https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/svd/svd.pdf?inline=true">Singular Value Decomposition</a></td> -->
  <!--   <td> -->
  <!--     <a href="https://youtube.com"><img src="/pix/icons/youtube.png"></a> -->
  <!--     <a href="./svd-sage.php"><img src="/pix/icons/sage.svg"></a> -->
  <!--   </td> -->
  <!-- </tr> -->

</table>
